# foss-stories

Stories of what it's like to develop and maintain free and open source
software.

Most people don't know what it's like to maintain an open source
program. This repository collects stories from the lives of free and
open source maintainers.

Please submit your story, if you maintain any free and open source
software!

* It doesn't matter what size or how popular your project is.
* It doesn't matter how long you've been maintaining it.
* Your story can be as long or short as you wish.
* Open an issue or a merge request against this repository, or send
  mail to privately (`liw@liw.fi`).
* If you prefer to be anonymous, send me email and I'll strip your
  name.
* I would prefer markdown, and licensed as CC-BY-SA 4.0 (Unported).

For the purposes of this repository, "maintaining" means responding to
issues, fixing bugs, and generally keeping the project alive.
